package model.data_structures;

import java.util.Iterator;

//TODO hacer el UML de esta clase.
/**
 * Clase de lista circular gen�rica.
 * @author 
 *
 * @param <T>
 */
public class RingList<T> implements IList{

	private class Node{

		private Node next;

		private Node previous;

		private T item;
	}
/**
 * Clase iterador de la lista
 * @author
 *
 * @param <T>
 */
	private class RingListIterator<T> implements Iterator<T>{

		/**
		 * Nodo actual del iterador
		 */
		private Node current = first;

		private boolean throughFirst = false;

		/**
		 * M�todo para verificar si el nodo actual tiene uno siguiente
		 */
		public boolean hasNext()
		{
			if(current == null)
				return false;
			else if(current == first && throughFirst)
				return false;
			else if(current == first && !throughFirst)
				throughFirst = true;
			return true;
		}

		/**
		 * M�todo que retorna el nodo actual y posteriormente lo actualiza por su siguiente
		 * @return item: item del nodo actual
		 */
		public T next() {
			T item = (T) current.item;
			current = current.next;
			return item;
		}

	}

	/**
	 * Primer nodo de la lista
	 */
	private Node first;

	/**
	 * Nodo actual de la lista
	 */
	private Node current;

	/**
	 * Tama�o de la lista
	 */
	private int size = 0;
	
	//-----------------
	//M�todos
	//-----------------
	
	/**
	 * M�todo que inicializa y retorna el iterador
	 * @return iterator: iterador de la lista
	 */
	public Iterator<T> iterator() {
		return new RingListIterator();
	}
	/**
	 * M�todo que retorna el tama�o de la lista
	 * @return size: tama�o de la lista
	 */
	public Integer getSize() {
		return size;
	}

	/**
	 * M�todo que a�ade el objeto recibido por parametro a la lista
	 * @param item: objeto por a�adir a la lista
	 */
	public void add(Object item) {

		Node n = new Node();
		n.item = (T) item;

		if (first == null)
		{
			first = n;
			first.next = first;
			first.previous = first;
			current = first;
		}

		else{
			Node last = first.previous;

			n.next = first;

			first.previous = n;	

			n.previous = last;
			last.next = n;
			first = n; 
			//current = first; revisar esto
		}


		++size;
	}



	/**
	 * M�todo que a�ade el objeto recibido por parametro al final de la lista
	 * @param item: objeto por a�adir a la lista
	 */
	public void addAtEnd(Object item) {
		Node nuevo = new Node( );
		nuevo.item = (T) item;
		if ( first == null)
		{
			first = nuevo;
			first.next = first;
			first.previous = first;
		}
		else
		{
			// xult es el ex-ultimo nodo
			Node xult = first.previous;
			xult.next = nuevo;
			first.previous = nuevo;
			nuevo.next = first;
			nuevo.previous = xult;
		}

		if(current == null)
		{
			current = first;
		}

		++size;

	}

	/**
	 * M�todo que a�ade el objeto recibido por par�metro a la posici�n recibida por par�metro.
	 * @param pos: posici�n en la que se desea a�adir.
	 * @param item: item por a�adir a la lista.
	 */
	public void addAtK(int pos, Object item) {
		int count = 0;

		Node n;

		Node n1 = new Node();
		n1.item = (T) item;

		if(size != 0)
			pos = (int) (pos%size);

		boolean dir;
		if (pos == 0)
			dir = true;
		else dir = (size / pos >=2)? true : false; //Si es igual a 2, el objeto está justamente en la mitad de la lista. Si es mayor está más cerca a first que a last.
		//dir true recorre hacia la izquierda, false hacia la derecha.

		n = first;

		if(dir){ //Si true recorre de izquierda a derecha.

			while(count++<pos)
			{
				n = n.next;
			}
		}
		else {//si false recorre de izquierda a derecha. objetivo, recucir la complejidad algor�tmica en el peor de los casos a n/2
			pos = (int)(size-pos);
			while(count++<pos)
			{
				n = n.previous;
			}
		}

		Node previous = n.previous;

		if ( first == null)
		{
			first = n1;
			first.next = first;
			first.previous = first;
		}
		else {
			n1.previous = previous;
			n1.next = n;
			n.previous = n1;
			previous.next = n1; 
		}
		if(n == first)
			first = n1;

		if(current == null) current = first;

		++size;

	}

	/**
	 * M�todo que retorna el objeto de la posici�n recibida por par�metro
	 * @param pos: posici�n de la que se quiere obtener el objeto
	 * @throws Exception
	 */
	@Override
	public Object getElement(int pos) throws Exception{
		int count = 0;

		Node n;

		boolean dir;


		if(size == 0)
			throw new Exception("Cann't access an empty RingList");

		pos = (int) (pos%size);


		if(pos == 0)
			dir = true;
		else if(size/pos >= 2)//Si es igual a 2, el objeto está justamente en la mitad de la lista. Si es mayor está más cerca a first que a last.
			dir = true;
		else {
			dir = false;
			pos = (int) (size - pos ); //Se buscará de derecha a izquierda, por lo tanto pos debe ser la distancia de derecha a izquierda.
		}

		n = first;

		if(dir){ //Si true recorre de izquierda a derecha.

			while(count++<pos)
			{
				n = n.next;
			}
		}
		else {//si false recorre de izquierda a derecha. objetivo, recucir la complejidad algor�tmica en el peor de los casos a n/2
			while(count++<pos)
			{
				n = n.previous;
			}
		}
		return n.item;
	}

	/**
	 * M�todo que retorna el elemento actual de la lista
	 */
	public T getCurrentElement() {
		if(current != null)
			return current.item;
		return null;
	}

	/**
	 * M�todo que elimina el elemento actual de la lista.
	 * @throws Exception
	 */
	public void delete() throws Exception{
		if(size > 1)
		{
			Node previous = current.previous;
			Node next = current.next;

			previous.next = next;
			next.previous = previous;

			if(current == first)
				first = next;
			current = current.next;
		} 
		else if(size == 1)
		{
			first = null;
			current = null;
		}
		else
			throw new Exception("Cannot delete, the list is empty");
		--size;

	}

	/**
	 * M�todo que elimina el objeto de la posici�n recibida por parametro.
	 * @param pos: posici�n de la que se desea eliminar el objeto
	 * @throws Exception
	 */
	public void deleteAtPos(int pos) throws Exception {

		boolean dir;
		int counter = 0;
		Node n = first;

		if(size == 0)
			throw new Exception("Cann't access an empty RingList");
		pos = (pos%size);


		if(pos == 0)
			dir = true;
		else if(size/pos >= 2)//Si es igual a 2, el objeto está justamente en la mitad de la lista. Si es mayor está más cerca a first que a last.
			dir = true;
		else {
			dir = false;
			pos = (int) (size - pos); //Se buscará de derecha a izquierda, por lo tanto pos debe ser la distancia de derecha a izquierda.
		}

		if(dir)
		{
			while(counter++ < pos)
			{
				n = n.next;
			}
		}
		else {
			while(counter++<pos)
			{
				n = n.previous;
			}


		}

		Node previous = n.previous;
		Node next = n.next;

		if(size>1) {
			previous.next = next;
			next.previous = previous;
		}
		else { //si sólo hay un elemento se hace first = null para eliminarlo.
			first = null;
		}

		if(n == first)
			first = n.next;

		if(n == current) //Si voy a eliminar el current, lo corro hacia la izquierda 
			current = current.next;

		if(size == 1)
			current = null;

		--size;

	}

	/**
	 * M�todo que actualiza actual por su siguiente.
	 */
	public T next() {
		if(current!=null)
			current = current.next;
		else
			current = null;

		if(current!= null)
			return current.item;
		return null;
	}

	/**
	 * M�todo que actualiza actual por su anterior.
	 */
	public T previous() {
		if(current != null)
			current = current.previous;
		else //si current == null, la lista está vacía.
			current = null;

		if(current!= null)
			return current.item;
		return null;
	}

	/**
	 * M�todo que retorna el primer objeto de la lista.
	 * @return Primer objeto de la lista.
	 */
	public T getFirst()
	{
		return first.item;
	}
	
	/**
	 * M�todo que obtiene el nodo de la posici�n recibida por par�metro.
	 * @param pos: posici�n de la que se quiere obtener el Nodo
	 * @return nodo de la posici�n
	 * @throws Exception
	 */
	private Node getNode(int pos) throws Exception
	{
		int count = 0;

		Node n;

		boolean dir;


		if(size == 0)
			throw new Exception("Cann't access an empty RingList");

		pos = (int) (pos%size);


		if(pos == 0)
			dir = true;
		else if(size/pos >= 2)//Si es igual a 2, el objeto está justamente en la mitad de la lista. Si es mayor está más cerca a first que a last.
			dir = true;
		else {
			dir = false;
			pos = (int) (size - pos ); //Se buscará de derecha a izquierda, por lo tanto pos debe ser la distancia de derecha a izquierda.
		}

		n = first;

		if(dir){ //Si true recorre de izquierda a derecha.

			while(count++<pos)
			{
				n = n.next;
			}
		}
		else {//si false recorre de izquierda a derecha. objetivo, recucir la complejidad algor�tmica en el peor de los casos a n/2
			while(count++<pos)
			{
				n = n.previous;
			}
		}
		return n;
	}


	/**
	 * Actualiza el Nodo actual por el de la posici�n recibida por par�metro
	 * @param pos: Posici�n en la cual se encuentra el nodo que se quiere fijar como el actual.
	 * @throws Exception
	 */
	public void setCurrent(int pos) throws Exception
	{
		current = getNode(pos);
	}
}
