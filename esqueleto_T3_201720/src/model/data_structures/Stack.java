package model.data_structures;

/**
 * Representa una pila.
 * @param <E> tipo de elementos que contendrá la pil
 */
public class Stack<E > implements IStack<E>{

	/**
	 * Nodo que contiene el siguiente elemento de la lista y el item que le corresponde.
	 */
	private class Node{

		private Node next;

		private E item;

	}
	
	/**
	 * Representa la primera posición de la estructura, de donde se eliminarán y añadirán archivos.
	 */
	private Node first;

	/**
	 * Representa el tamaño de la estructura.
	 */
	private int size;

	/**
	 * Elimina el nodo de la primera posición de la estructura y devuelve su valor.
	 * @return E retorna el item del nodo que se acaba de eliminar.
	 */
	public E pop() {
		if(first == null) return null;
		else {
			E item = first.item;
			first = first.next;
			--size;
			return item;
		}
	}

	/**
	 * Asigna el item a un nuevo nodo y se agrega el nodo al principio de la pila.
	 */
	public void push(E item) {
		Node n = new Node();
		n.item = item;
		
		n.next = first;
		first = n;


		++size;

	}

	/**
	 * Retorna el tamaño de la estructura
	 * @return el tamaño de la estructura.
	 */
	public int size() {
		return size;
	}
}
