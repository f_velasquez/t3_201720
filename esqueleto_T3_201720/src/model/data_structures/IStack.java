package model.data_structures;

public interface IStack<E> {

	public void push (E item);
	
	public E pop();
	
	public int size();
}
