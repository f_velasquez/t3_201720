package model.data_structures;

/**
 * Representa una cola.
 */
public class Queue<T> implements IQueue<T>{

	/**
	 * Nodo que contiene el siguiente elemento de la lista y el item que le corresponde.
	 */
	private class Node{
		private Node next;
		private T item;
	}

	/**
	 * Representa el primer nodo de la estructura, donde se hace dequeue
	 */
	private Node first;

	/**
	 * Representa el último nodo de la estructura, donde se hace queues
	 */
	private Node last;

	/**
	 * Representa el tamaño de la estructura
	 */
	private int size=0;

	/**
	 * Añade un nodo al final de la estructura.
	 */
	public void enqueue(T item) {

		Node n = new Node();
		n.item = item;

		if(last == null)
			first = last = n;
		else {
			last.next = n;
			last = n;
		}
		++size;
	}

	/**
	 * Saca el primer nodo de la estructura 
	 * @return el item del nodo que se sacó de la estrucutra.
	 */
	public T dequeue() {

		if(first == null) return null;
		
		T item = first.item;

		first = first.next;
		
		--size;

		return item;


	}

	/**
	 * Retorna el tamaño de la cola
	 * @return tamaño de la cola.
	 */
	public int size() {
		return size;
	}
}
