package model.vo;

public class BusUpdateVO {


	private class RouteMap{
		private String Href;
		private RouteMap(String Href) {
			this.Href = Href;
		}
		
	}
	
	//------------------
	//Atributos
	//------------------
	private String VehicleNo;
	
	private int TripId;
	
	private String RouteNo;
	
	private String Direction;
	
	private String Destination;
	
	private double Latitude;
	
	private double Longitude;
	
	private String RecordedTime;
	
	private RouteMap RouteMap;
	
	/**
	 * M�todo constructor de la clase BusUpdateVO
	 * @param vehicleNo
	 * @param tripId
	 * @param routeNo
	 * @param direction
	 * @param destination
	 * @param latitude
	 * @param longitude
	 * @param recordedTime
	 * @param routeMap
	 */
	public BusUpdateVO(String VehicleNo, int TripId, String RouteNo, String Direction, String Destination, double Latitude, double Longitude, String RecordedTime, RouteMap RouteMap )
	{
		this.VehicleNo = VehicleNo;
		this.TripId = TripId;
		this.RouteNo = RouteNo;
		this.Direction = Direction;
		this.Destination = Destination;
		this.Latitude = Latitude;
		this.Longitude = Longitude;
		this.RecordedTime = RecordedTime;
		this.RouteMap = RouteMap;
	}
	
	public int tripId()
	{
		return TripId;
	}

	public String vehicleNo()
	{
		return VehicleNo;
	}
	
	public double lat()
	{
		return Latitude;
	}
	
	public double lon() {
		
	
	return Longitude;
	}
}
