package model.vo;

/**
 * Representation of a Stop object
 */
public class VOStop{
	
	/**
	 * Representa el stopId.
	 */
	private int id;
	
	/**
	 * Representa el código de la parada.
	 */
	private Integer stopCode;
	
	/**
	 * Representa el nombre de la parada.
	 */
	private String stopName;
	
	/**
	 * Representa la descripción de la parada.
	 */
	private String stopDesc;
	
	/**
	 * Representa la latitud de la parada.
	 */
	private double stopLat;
	
	/**
	 * Representa la longitud de la parada.
	 */
	private double stopLon;
	
	/**
	 * Representa el ID de la zona de la parada.
	 */
	private String zoneId;
	
	/**
	 * Representa el url con información de la parada.
	 */
	private String stopUrl;
	
	/**
	 * Representa el tipo de locación.
	 */
	private byte locationType;
	
	/**
	 * Representa la estación padre (si tiene)
	 */
	private String parentStation;
	
	/**
	 * Construye un objeto de esta clase<br>
	 * <b>Post:</b> Se ha construido en VOStop con su información correspondiente.
	 * @param id id de la parada.
	 * @param stopCode código de la parada.
	 * @param stopName nombre de la parada.
	 * @param stopDesc descripción de la parada.
	 * @param stopLat latitud de la parada.
	 * @param stopLon longitud de la parada.
	 * @param zoneId id de la zona donde está la parada.
	 * @param stopUrl url con información de la parada.
	 * @param locationType tipo de locaciónm
	 * @param parentStation estación padre, si tiene.
	 */
	public VOStop(int id, Integer stopCode, String stopName, String stopDesc, double stopLat, double stopLon, String zoneId, String stopUrl, byte locationType, String parentStation)
	{
		this.id = id;
		this.stopCode = stopCode;
		this.stopName = stopName;
		this.stopDesc = stopDesc;
		this.stopLat = stopLat;
		this.stopLon = stopLon;
		this.zoneId = zoneId;
		this.stopUrl = stopUrl;
		this.locationType = locationType;
	}
	
	/**
	 * Retorna el id de la parada.
	 * @return el id de la parada.
	 */
	public int id() {
		return id;
	}
	
	/**
	 * Retorna el código de la parada
	 * @return el código de la parada.
	 */
	public int stopCode() {
		return stopCode;
	}
	
	/**
	 * Retorna la descripción de la parada.
	 * @return descripción de la parada
	 */
	public String stopDesc() {
		return stopDesc;
	}
	
	/**
	 * Retorna la latitud de la parada
	 * @return la latitud de la parada.
	 */
	public double stopLat() {
		return stopLat;
	}
	
	/**
	 * Retorna la longitud de la parada
	 * @return la longitud de la parada.
	 */
	public double stopLon() {
		return stopLon;
	}
	
	/**
	 * Retorna el id la zona de la parada
	 * @return el id de la zona de la  parada.
	 */
	public String zoneId() {
		return zoneId;
	}
	
	/**
	 * Retorna lel url de la parada.
	 * @return lurl de la parada.
	 */
	public String stopUrl() {
		return stopUrl;
	}
	
	/**
	 * Retorna la el tipo de locación
	 * @return el tipo de locación
	 */
	public byte locationType() {
		return locationType;
	}
	
	/**
	 * Retorna el nombre de la estación padre.
	 * @return el nombre de la estación padre
	 */
	public String parentStation() {
		return parentStation;
	}
	

	/**
	 * @return el nombre de la parada.
	 */
	public String getName() {
		return stopName;
	}

}
