package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;

import java.util.Iterator;

import java.io.InputStream;
import java.io.InputStreamReader;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

import model.data_structures.IStack;
import model.data_structures.Queue;
import model.data_structures.RingList;
import model.data_structures.Stack;
import model.vo.BusUpdateVO;
import model.vo.VOStop;
import api.ISTSManager;

public class STSManager implements ISTSManager{


	private RingList<VOStop> stops;

	private Queue<BusUpdateVO> busUp = new Queue<BusUpdateVO>();

	private static final String stopsFile = "stops.txt";


	@Override
	public void readBusUpdate(File rtFile) {

		try {

			Gson gson = new GsonBuilder().create();

			InputStream stream = new FileInputStream(rtFile);

			JsonReader reader = new JsonReader(new InputStreamReader(stream, "UTF-8"));


			reader.beginArray();
			while(reader.hasNext())
			{
				BusUpdateVO add = gson.fromJson(reader, BusUpdateVO.class);
				busUp.enqueue(add);

			}

			reader.close( );
		}
		catch(Exception e) {
			e.printStackTrace();
		}

		System.out.println(busUp.size());
	}

	//Pre: stops y busUp han sido inicializados.
	public IStack<VOStop> listStops(Integer tripID) {
		
		BusUpdateVO tmp = null;
		VOStop tmpStp = null;
		VOStop tmpStp2 = null;

		Iterator<VOStop> it;


		Stack<VOStop> stops = new Stack<VOStop>();

		tmp = busUp.dequeue();
		//System.out.println(tmp.tripId());
		while(tmp != null)
		{
			//System.out.println(tmp.tripId());
			if(tmp.tripId() == tripID)
			{
				it = this.stops.iterator();
				while(it.hasNext())
				{
					tmpStp = it.next();
					if(getDistance(tmp.lat(), tmp.lon(), tmpStp.stopLat(), tmpStp.stopLon()) <= 70 ) {

						//stops.push(tmpStp);

						//Para no añadir la misma parada varias veces seguida.

						if(stops.size() == 0)
						{
							//System.out.println("añadió" + tmpStp.getName());
							stops.push(tmpStp);
						}
						else {
							tmpStp2 = stops.pop();
							//System.out.println(tmpStp2.getName() + " vs " + tmpStp.getName());
							if(!(tmpStp2.getName().equals(tmpStp.getName())))
							{
								stops.push(tmpStp2);
								stops.push(tmpStp);
							}
							else
								stops.push(tmpStp2);
						}
					}

				}
			}

			tmp = busUp.dequeue();
		}

		busUp = new Queue<BusUpdateVO>();
		return stops;
	}

	@Override
	public void loadStops() {

		stops = new RingList<VOStop>();

		System.out.println("Leyendo archivo, esto puede tomar un momento");

		try
		{
			BufferedReader bf = new BufferedReader( new FileReader( new File(stopsFile) ) );
			String ln = bf.readLine( );
			ln = bf.readLine( );

			VOStop stop;

			boolean termino = false;
			int i;
			VOStop comp;
			int size;
			String inf9;
			String[] info;
			String zoneId;
			Integer inf1;
			while ( ln != null )
			{ 
				info= ln.split(",");
				zoneId = info[6];

				try {
					inf9 = info[9].trim();
				}
				catch(ArrayIndexOutOfBoundsException ae) {
					inf9 = null;
				}
				try {
					inf1 = Integer.parseInt(info[1]);
				}
				catch(NumberFormatException ne)
				{
					inf1 = null;
				}

				stop = new VOStop(Integer.parseInt(info[0]), inf1, info[2].trim(), info[3].trim(), Double.parseDouble(info[4]), Double.parseDouble(info[5]), info[6].trim(), info[7].trim(), Byte.parseByte(info[8]),inf9);

				if(stops.getSize( ) == 0)
				{
					stops.addAtEnd(stop);
				}
				else
				{
					//Organiza las zonas alfanuméricamente y después por nombres de los stops
					termino = false;
					i = 0;
					stops.setCurrent(0);
					size = stops.getSize();
					while (i < size && !termino)
					{
						comp = (VOStop) stops.getCurrentElement();
						if(!( comp.zoneId().compareTo(zoneId)<= 0) && !(comp.zoneId().equals(zoneId))){
							stops.addAtK(i, stop);
							termino = true;
						}
						else if(comp.zoneId().equals(zoneId))
						{
							if(comp.getName().compareTo(stop.getName())>=0)
							{
								stops.addAtK(i, stop);
								termino = true;
							}
						}

						i++;
						stops.next();
					}
					if(!termino)
					{
						stops.addAtEnd(stop);
					}
				}
				ln = bf.readLine( );
			}
			bf.close();

			/*int a = 0;
			VOStop t;
			while(a<stops.getSize())
			{
				t = (VOStop) stops.getElement(a);
				System.out.println(t.zoneId() + " : " +t.getName());
				a++;
			}*/


			System.out.println("Terminó de leer archivo de texto");
			
		}



		catch( Exception e )
		{
			e.printStackTrace();
			System.out.println("Unexpected error loading the stops file");
		}

		System.out.println("tamaño de la lista: " +stops.getSize());


	}

	private double getDistance(double lat1, double lon1, double lat2, double lon2) {
		final int R = 6371*1000; // Radious of the earth
		Double latDistance = toRad(lat2-lat1);
		Double lonDistance = toRad(lon2-lon1);
		Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		Double distance = R*c;
		return distance;
	}

	private Double toRad(Double value) {
		return value * Math.PI / 180;
	}

}
