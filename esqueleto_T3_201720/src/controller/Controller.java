package controller;

import java.io.File;

import model.data_structures.IStack;
import model.exceptions.TripNotFoundException;
import model.logic.STSManager;
import model.vo.BusUpdateVO;
import model.vo.VOStop;
import api.ISTSManager;

public class Controller {
	
	/**
	 * Reference to the routes and stops manager
	 */
	private static ISTSManager  manager = new STSManager();

	public static void loadStops() {
		manager.loadStops();		
	}

	public static void readBusUpdates() {
		File f;;
		//File[] updateFiles = f.listFiles();
		for (int i = 0; i < 1000; i++) {
			f = new File("data"+File.separator+"BUSES_SERVICE_" + i +".json");
			manager.readBusUpdate(f);
			System.out.println(i);
		}
	}
	
	public static void listStops(Integer tripId) throws TripNotFoundException{
		
		IStack<VOStop> a = manager.listStops(tripId);
		
		if(a.size() == 0)
			throw new TripNotFoundException();
		else {
			int i = 0;
			while (i<a.size())
			{
				System.out.println(a.pop().getName());
			}
		}
		
		System.out.println("Si desea volver a llamar este método, porfavor vuelva a cargar las actualizaciones de los buses");
		
	}
	

}
