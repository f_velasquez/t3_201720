package t3_201720;

import junit.framework.TestCase;
import model.data_structures.Stack;

public class StackTest extends TestCase{

	private Stack<Integer> stack;
	
	private void setUpEscenario1()
	{
		stack = new Stack<Integer>();
		
		stack.push(0);
		stack.push(1);
		stack.push(2);
		stack.push(3);
		stack.push(4);
		stack.push(5);
		stack.push(6);
		stack.push(7);
		stack.push(8);
		stack.push(9);
	}
	
	//Caso 1: el stack está vacía.
	//Caso 2:el stack no está vacía.
	public void testPop()
	{
		//Caso 1:
		stack = new Stack<Integer>();
		assertNull(stack.pop());
		assertEquals(stack.size(), 0);
		
		//Caso 2:
		setUpEscenario1();
		assertEquals(stack.pop(), new Integer(9));
		assertEquals(stack.size(), 9);
	}
	
	//Caso 1: el stack está vacía.
	//Caso 2: el stack no está vacía.
	public void testPush()
	{
		//Caso 1.
		stack = new Stack<Integer>();
		stack.push(0);
		assertEquals(stack.size(),1);
		assertEquals(stack.pop(), new Integer(0));
		 
		//Caso 2:
		setUpEscenario1();
		stack.push(10);
		assertEquals(stack.size(), 11);
		assertEquals(stack.pop(), new Integer(10));
	}
	
	//Caso 1: el stack está vacío
	//Caso 1A : el stack no está vacío.
	//Caso 2: se añadió un elemento al stack.
	//Caso 3: se removió un elemento del stack
	public void testGetSize() {
		
		//Caso 1:
		stack = new Stack<Integer>();
		assertEquals(0, stack.size());
		
		//Caso 1A:
		setUpEscenario1();
		assertEquals(10, stack.size());
		
		//Caso 2: 
		stack.push(new Integer(10));
		assertEquals(11, stack.size());
		
		//Caso 3:
		stack.pop();
		assertEquals(10, stack.size());
		
	}
}


