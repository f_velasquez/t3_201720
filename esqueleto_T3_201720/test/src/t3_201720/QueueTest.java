package t3_201720;

import junit.framework.TestCase;
import model.data_structures.Queue;
import model.data_structures.Stack;

public class QueueTest extends TestCase{

	private Queue<Integer> queue;

	private void setUpEscenario1()
	{
		queue = new Queue<Integer>();
		queue.enqueue(0);
		queue.enqueue(1);
		queue.enqueue(2);
		queue.enqueue(3);
		queue.enqueue(4);
		queue.enqueue(5);
		queue.enqueue(6);
		queue.enqueue(7);
		queue.enqueue(8);
		queue.enqueue(9);
	}

	//Caso 1: la lista está vacía -> first = last después de ser añadido 
	//Caso 2: la lista no está vacía, hacer dequeue debería retornar un elemento distinto al añadido más recientemente.
	public void testEnqueue(){

		//Caso 1:
		queue = new Queue<Integer>();
		queue.enqueue(0);

		assertEquals(queue.size(),1);
		assertEquals(queue.dequeue(), new Integer(0));

		//Caso 2:
		setUpEscenario1();

		queue.enqueue(10);
		assertEquals(queue.size(), 11);
		assertFalse(queue.dequeue().equals(new Integer(11)));
	}

	//Caso 1: la lista está vacía
	//Caso 2: queue no está vacía
	public void testDequeue() {

		//Caso 1:
		queue = new Queue<Integer>();

		assertNull(queue.dequeue());
		assertEquals(queue.size(),0);

		//Caso 2:
		setUpEscenario1();

		assertEquals(queue.dequeue(), new Integer(0));
		assertEquals(queue.size(), 9);

	}

	//Caso 1: el stack está vacío
	//Caso 1A : el stack no está vacío.
	//Caso 2: se añadió un elemento al stack.
	//Caso 3: se removió un elemento del stack
	public void testGetSize() {

		//Caso 1:
		queue = new Queue<Integer>();
		assertEquals(0, queue.size());

		//Caso 1A:
		setUpEscenario1();
		assertEquals(10, queue.size());

		//Caso 2: 
		queue.enqueue(new Integer(10));
		assertEquals(11, queue.size());

		//Caso 3:
		queue.dequeue();
		assertEquals(10, queue.size());
	}
}
